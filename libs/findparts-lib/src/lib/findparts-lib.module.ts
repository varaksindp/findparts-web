import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { FindpartsLibComponent } from './findparts-lib.component';
import { PartnersComponent } from './components/partners/partners.component';
import { RouterModule } from '@angular/router';
import { FindpartsRoutingModule } from './findparts-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TermsComponent } from './components/terms/terms.component';
import { ContactDialogComponent } from './components/footer/contact-dialog/contact-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { PartsFormComponent } from './components/main-layout/parts-form/parts-form.component';
import { TyresFormComponent } from './components/main-layout/tyres-form/tyres-form.component';
import { DetailingFormComponent } from './components/main-layout/detailing-form/detailing-form.component';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { HttpClientModule } from '@angular/common/http';
import { FindpartsServiceApi } from './components/findparts.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { CookieService } from 'ngx-cookie-service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  RECAPTCHA_SETTINGS,
  RECAPTCHA_V3_SITE_KEY,
  RecaptchaFormsModule,
  RecaptchaModule, RecaptchaSettings,
  RecaptchaV3Module
} from 'ng-recaptcha';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FindpartsRoutingModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatCheckboxModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    RecaptchaV3Module
  ],
  declarations: [
    MainLayoutComponent,
    FindpartsLibComponent,
    PartnersComponent,
    HeaderComponent,
    FooterComponent,
    TermsComponent,
    ContactDialogComponent,
    PartsFormComponent,
    TyresFormComponent,
    DetailingFormComponent
  ],
  providers: [
    FindpartsServiceApi,
    CookieService,
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: '6LfBQpUaAAAAAE8N68gtUP2rdJbNNT_8w49_VIk2' }
  ],
  entryComponents: [ContactDialogComponent],
  exports: [FindpartsLibComponent]
})
export class FindpartsLibModule {}
