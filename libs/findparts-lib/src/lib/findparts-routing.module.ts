import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FindpartsLibComponent } from './findparts-lib.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { PartnersComponent } from './components/partners/partners.component';
import { TermsComponent } from './components/terms/terms.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: MainLayoutComponent },
  { path: 'partners', component: PartnersComponent },
  { path: 'terms', component: TermsComponent },
  { path: '**', redirectTo: ''}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FindpartsRoutingModule {}
