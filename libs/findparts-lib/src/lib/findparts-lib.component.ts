import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NavigationEnd, Router } from '@angular/router';
declare let jQuery: any;

@Component({
  selector: 'findparts-web-findparts-lib',
  templateUrl: './findparts-lib.component.html',
  styleUrls: ['./findparts-lib.component.scss']
})
export class FindpartsLibComponent implements OnInit, AfterViewInit {

  constructor(private cookieService: CookieService, private router: Router) { }
  @ViewChild('popup') popup: ElementRef;

  ngOnInit() {
    // stickynavbar on scroll
    (function ($) {
      $(document).ready(function(){
        window.onscroll = function() {myFunction()};
        var header = document.getElementById('myNavbar');
        var sticky = header.offsetTop;
        function myFunction() {
          if (window.pageYOffset > sticky) {
            header.classList.add('sticky');
            $('#main').addClass('slide-margin');
          } else {
            header.classList.remove('sticky');
            $('#main').removeClass('slide-margin');
          }
        }
      });
    })(jQuery);

    // scrolling to top on page change
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        window.scrollTo(0, 0)
      }
    });
  }

  ngAfterViewInit() {
    // checks if cookie 'fp_consent' set, if not displays popup by removing hidden class
    setTimeout( () => {
      if(!this.cookieService.check('fp_consent')) {
        setTimeout(() => {
          this.popup.nativeElement.classList.add('show-consent');
        }, 1000);
      }
    }, 1000);
  }

  acceptCookies() {
    this.cookieService.set( 'fp_consent', 'accepted' );
    this.popup.nativeElement.classList.remove('show-consent');
  }
}
