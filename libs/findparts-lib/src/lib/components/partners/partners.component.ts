import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'findparts-web-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit, AfterViewInit {

  constructor() { }
  partnersForm: FormGroup;
  showButton = true;
  @ViewChild('partHeadCont') partHeadCont: ElementRef;

  ngOnInit() {
    // Partners form init
    this.partnersForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      serviceType: new FormControl('', [Validators.required])
    })
  }

  submit() {
    // TODO add backend
    console.log('Form submitted');
  }

  ngAfterViewInit() {
    setTimeout(()  => {
      this.partHeadCont.nativeElement.classList.add('visible');
    }, 500)
  }
}
