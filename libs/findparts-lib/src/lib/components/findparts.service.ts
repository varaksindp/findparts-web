import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class FindpartsServiceApi {
  url = 'http://192.168.1.3:80';
  SITE_KEY = '6LfBQpUaAAAAAE8N68gtUP2rdJbNNT_8w49_VIk2';

  constructor(private http: HttpClient) {}

  addDetailingRequest(request) {
    return this.http.post(`${this.url}/api/detailing`, request);
  }

  addPartsRequest(request) {
    return this.http.post(`${this.url}/api/parts`, request);
  }

  addTyresRequest(request) {
    return this.http.post(`${this.url}/api/tyres`, request);
  }

  handshake() {
    return this.http.get(`${this.url}/api/handshake`);
  }

  fileuploadHandshake() {
    return this.http.get(`${this.url}/api/fileupload/handshake`);
  }

  partsPhotoUpload(fd, headers){
    return this.http.post(`${this.url}/api/fileupload`, fd, headers);
  }

  getSiteKey() {
    return this.SITE_KEY;
  }
}
