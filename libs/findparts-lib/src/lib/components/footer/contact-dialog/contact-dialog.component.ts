import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FindpartsServiceApi } from '../../findparts.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'findparts-web-contact-dialog',
  templateUrl: './contact-dialog.component.html',
  styleUrls: ['./contact-dialog.component.scss']
})
export class ContactDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ContactDialogComponent>,
    private findpartsApi: FindpartsServiceApi
  ) { }

  contactForm: FormGroup;

  ngOnInit() {
    this.contactForm = new FormGroup ({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }

  close() {
    this.dialogRef.close();
  }

  submit() {
    //TODO add form submit endpoint
    console.log(this.contactForm.value);
    this.dialogRef.close();
  }
}
