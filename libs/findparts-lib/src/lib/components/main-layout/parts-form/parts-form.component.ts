import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FindpartsServiceApi } from '../../findparts.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'findparts-web-parts-form',
  templateUrl: './parts-form.component.html',
  styleUrls: ['./parts-form.component.scss']
})
export class PartsFormComponent implements OnInit {

  constructor(private findpartsApi: FindpartsServiceApi) { }

  partsForm: FormGroup;
  selectedFile: File = null;
  selectedFileName;
  fileSizeCorrect = true;
  fileFormatCorrect = true;
  showButton = true;
  isLoading = false;
  isSuccess = false;
  isError = false;
  @Input() isCaptchaValid: boolean;

  ngOnInit() {
    this.partsForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      partsCondition: new FormControl('', [Validators.required]),
      carMake: new FormControl('', [Validators.required]),
      carModel: new FormControl('', [Validators.required]),
      yearOfProduction: new FormControl('', [Validators.required]),
      vin: new FormControl('', [Validators.required]),
      partName: new FormControl('', [Validators.required]),
      partNumber: new FormControl(''),
      notes: new FormControl(''),
      fileName: new FormControl({ value: '', disabled:true }),
      userConsent: new FormControl( false, [Validators.required])
    })

    // Contoller handshake
    this.findpartsApi.fileuploadHandshake().subscribe(
      next => {
        console.log(next, 'fileuploadHandshake next')
      },
      error => {
        console.log(error, 'fileuploadHandshake error');
      }
    );
  }

  submit() {
    this.showButton = false;
    this.isLoading = true;

    const partsRerBody = {
      "name": this.partsForm.value.name,
      "phone": this.partsForm.value.phone,
      "email": this.partsForm.value.email,
      "partsCondition": this.partsForm.value.partsCondition,
      "carMake": this.partsForm.value.carMake,
      "carModel": this.partsForm.value.carModel,
      "yearOfProduction": this.partsForm.value.yearOfProduction,
      "vinChassisNumber": this.partsForm.value.vin,
      "partName": this.partsForm.value.partName,
      "partNumber": this.partsForm.value.partNumber,
      "notes": this.partsForm.value.notes,
    }

    console.log('sent req', partsRerBody);

    // Sending FORM DATA
    if (this.isCaptchaValid) {
      this.findpartsApi.addPartsRequest(partsRerBody).subscribe(
        next => {
          console.log(next, 'FORM OK');
          setTimeout( () => {
            this.isLoading = false;
            this.isSuccess = true;
          }, 1000);
        },
        error => {
          console.log(error, 'FORM ERROR');
          setTimeout( () => {
            this.isLoading = false;
            this.showButton = true;
            this.isError = true;
          }, 1000);
        }
      );
      console.log('addPartsRequest send');

      // Sending FILE
      const fd = new FormData();
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');

      fd.append('file', this.selectedFile, this.selectedFile.name);
      this.findpartsApi.partsPhotoUpload(fd, headers).subscribe(
        res => {
          console.log(res);
        });
    }
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files.item(0);
    this.selectedFileName = event.target.files[0].name;

    // Checking file format or weight, if allowed set validation to true
    this.fileSizeCorrect = false;
    this.fileFormatCorrect = false;
    if (this.selectedFile.type === 'image/jpeg' || this.selectedFile.type === 'image/png') {
      this.fileFormatCorrect = true;
    }
    if (this.selectedFile.size <= 5779251) {
      this.fileSizeCorrect = true;
    }
  }
}
