import { Component, Input, OnInit } from '@angular/core';
import { FindpartsServiceApi } from '../../findparts.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'findparts-web-detailing-form',
  templateUrl: './detailing-form.component.html',
  styleUrls: ['./detailing-form.component.scss']
})
export class DetailingFormComponent implements OnInit {

  constructor(private findpartsApi: FindpartsServiceApi) { }

  detailingForm: FormGroup;
  showButton = true;
  isLoading = false;
  isSuccess = false;
  isError = false;
  @Input() isCaptchaValid: boolean;

  ngOnInit() {
    this.findpartsApi.handshake().subscribe(
      next => {
        console.log(next, 'handshake next')
      },
      error => {
        console.log(error, 'handshake error');
      }
    );

    this.detailingForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      service: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      notes: new FormControl(''),
      userConsent: new FormControl( false, [Validators.required])
    })
  }

  submit() {
    this.showButton = false;
    this.isLoading = true;

    const detailingRerBody = {
      "name": this.detailingForm.value.name,
      "phone": this.detailingForm.value.phone,
      "email": this.detailingForm.value.email,
      "service": this.detailingForm.value.service,
      "city": this.detailingForm.value.city,
      "notes": this.detailingForm.value.notes,
    }

    console.log('sent req', detailingRerBody);

    if (this.isCaptchaValid) {
      this.findpartsApi.addDetailingRequest(detailingRerBody).subscribe(
        next => {
          console.log(next, 'FORM OK');
          setTimeout( () => {
            this.isLoading = false;
            this.isSuccess = true;
          }, 1000);
        },
        error => {
          console.log(error, 'FORM ERROR');
          setTimeout( () => {
            this.isLoading = false;
            this.showButton = true;
            this.isError = true;
          }, 1000);
        }
      );
      console.log('addDetailingRequest send');
    }
  }
}
