import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FindpartsServiceApi } from '../../findparts.service';


@Component({
  selector: 'findparts-web-tyres-form',
  templateUrl: './tyres-form.component.html',
  styleUrls: ['./tyres-form.component.scss']
})
export class TyresFormComponent implements OnInit {

  constructor(private findpartsApi: FindpartsServiceApi) { }

  tyresForm: FormGroup;
  showButton = true;
  isLoading = false;
  isSuccess = false;
  isError = false;
  @Input() isCaptchaValid: boolean;

  ngOnInit() {
    this.tyresForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      tyreBrand: new FormControl('', [Validators.required]),
      tyreSize: new FormControl('', [Validators.required]),
      year: new FormControl(''),
      city: new FormControl('', [Validators.required]),
      notes: new FormControl(''),
      userConsent: new FormControl( false, [Validators.required])
    })
  }

  submit() {
    this.showButton = false;
    this.isLoading = true;

    const tyresReqBody = {
      "name": this.tyresForm.value.name,
      "phone": this.tyresForm.value.phone,
      "email": this.tyresForm.value.email,
      "tyreBrand": this.tyresForm.value.tyreBrand,
      "tyreSize": this.tyresForm.value.tyreSize,
      "year": this.tyresForm.value.year,
      "city": this.tyresForm.value.city,
      "notes": this.tyresForm.value.notes,
    }
    console.log('sent req', tyresReqBody);

    if (this.isCaptchaValid) {
      this.findpartsApi.addTyresRequest(tyresReqBody).subscribe(
        next => {
          console.log(next, 'FORM OK');
          setTimeout( () => {
            this.isLoading = false;
            this.isSuccess = true;
          }, 1000);
        },
        error => {
          console.log(error, 'FORM ERROR');
          setTimeout( () => {
            this.isLoading = false;
            this.showButton = true;
            this.isError = true;
          }, 1000);
        }
      );
      console.log('addPartsRequest send');
    }
  }
}
