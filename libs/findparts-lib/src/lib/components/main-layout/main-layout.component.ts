import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { logger } from '@nrwl/tao/src/shared/logger';
import { ContactDialogComponent } from '../footer/contact-dialog/contact-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { ReCaptchaV3Service } from 'ng-recaptcha';
declare let jQuery: any;

@Component({
  selector: 'findparts-web-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit, OnDestroy {

  constructor(public dialog: MatDialog, private router: Router, private recaptchaV3Service: ReCaptchaV3Service) { }
  @ViewChild('arrow1') arrow1: ElementRef;
  @ViewChild('arrow2') arrow2: ElementRef;
  @ViewChild('arrow3') arrow3: ElementRef;
  @ViewChild('heeaderFindpartsCont') heeaderFindpartsCont: ElementRef;
  urlChange: Subscription;
  isCaptchaValid: boolean;

  ngOnInit() {
    // TODO test it additionally on new host, comment in Amazon
    this.isCaptchaValid = true;
    // this.recaptchaV3Service.execute('partsFormSubmit').subscribe(
    //   (token) => {
    //     this.isCaptchaValid = true;
    //     console.log('captcha valid');
    //   },
    //   (error) => {
    //     this.isCaptchaValid = false;
    //     console.log('captcha invalid');
    //   }
    // );

    (function ($) {
      $( "#partsBtnParts" ).click(function() {
        if( $('#partsFormParts').css('display') == 'block' ) {
          // do nothing
        } else {
          $(".formsCollapse").hide();
          $( "#partsFormParts" ).slideDown( "slow", function() {});
        }
      });


      $( "#partsBtnTyres" ).click(function() {
        if( $('#partsFormTyres').css('display') == 'block' ) {
          // do nothing
        } else {
          $(".formsCollapse").hide();
          $( "#partsFormTyres" ).slideDown( "slow", function() {});
        }
      });

      $( "#partsBtnDetailing" ).click(function() {
        if( $('#partsFormDetailing').css('display') == 'block' ) {
          // do nothing
        } else {
          $(".formsCollapse").hide();
          $( "#partsFormDetailing" ).slideDown( "slow", function() {});
        }
      });
    })(jQuery);
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    // adding bounce when scrolled to from buttons
    const offset = 150;
    const bodyRect = document.body.getBoundingClientRect().top;
    const heeaderFindpartsCont = this.heeaderFindpartsCont.nativeElement.getBoundingClientRect().top;
    const contPosition = heeaderFindpartsCont - bodyRect;
    const offsetPosition = contPosition - offset;
    const bodyRectPositive = bodyRect * -1;

    if (bodyRectPositive > offsetPosition) {
      this.arrow1.nativeElement.classList.add('bounce');
      this.arrow2.nativeElement.classList.add('bounce');
      this.arrow3.nativeElement.classList.add('bounce');
    }
  }

  openDialog() {
    this.dialog.open(ContactDialogComponent);
  }

  scrollToForms(el: HTMLElement) {
    const offset = 0;
    const bodyRect = document.body.getBoundingClientRect().top;
    const elementRect = el.getBoundingClientRect().top;
    const elementPosition = elementRect - bodyRect;
    const offsetPosition = elementPosition - offset;

    window.scrollTo({
      top: elementPosition,
      behavior: 'smooth'
    });
  }

  ngOnDestroy() {
  }
}
