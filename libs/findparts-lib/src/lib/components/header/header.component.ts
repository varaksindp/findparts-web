import { Component, OnInit } from '@angular/core';
declare let jQuery: any;

@Component({
  selector: 'findparts-web-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    (function ($) {
      $(document).ready(function(){
        window.onscroll = function() {myFunction()};
        var header = document.getElementById('myNavbar');
        var sticky = header.offsetTop;
        function myFunction() {
          if (window.pageYOffset > sticky) {
            header.classList.add('sticky');
            $('#main').addClass('slide-margin');
          } else {
            header.classList.remove('sticky');
            $('#main').removeClass('slide-margin');
          }
        }
      });
    })(jQuery);
  }

}
